package file;

import java.io.*;

// This code shows how to process all lines of a text file.

	public class File1 {
	public static void readFile(String filePath) {
		try (BufferedReader r = new BufferedReader(new FileReader(filePath))) {
			r.lines().forEach(l -> System.out.println(l));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		readFile("data/The Lion's Story.txt");
	}
}