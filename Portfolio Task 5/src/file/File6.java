package file;

import java.io.*;

public class File6 {

public static String concatenateLines(BufferedReader reader) throws Exception {
return reader.lines().reduce("", String::concat);
}

public static void main(String[] args) throws Exception {
BufferedReader r = new BufferedReader(new FileReader("data/The Lion's Story.txt"));
System.out.println(concatenateLines(r));
r.close();
}
}