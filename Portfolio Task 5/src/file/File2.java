package file;

import java.io.*;

// This program counts the number of lines in a text file.

	public class File2 {

		public static void main(String[] args) {
			try (BufferedReader r = new BufferedReader(new FileReader("data/The Lion's Story.txt"))) {
				System.out.println(r.lines().count());
			} catch (Exception e) {
				e.printStackTrace();
		}
	}
}