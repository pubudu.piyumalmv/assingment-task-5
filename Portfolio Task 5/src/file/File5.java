package file;

import java.io.*;
import java.util.Comparator;

public class File5 {

  public static void sortLines(BufferedReader reader) throws Exception {
    reader.lines().sorted(new LineLengthComparator()).forEach(l -> System.out.println(l));
  }

  static class LineLengthComparator implements Comparator<String> {
    @Override
    public int compare(String a, String b) {
      if (a.length() == b.length()) {
        return 0;
      } else if (a.length() < b.length()) {
        return 1;
      } else {
        return -1;
      }
    }
  }

  public static void main(String[] args) throws Exception {
    BufferedReader r = new BufferedReader(new FileReader("data/The Lion's Story.txt"));
    sortLines(r);
    r.close();
  }
}
