package file;

import java.io.*;

// This program uses a map to convert a text file to upper case.

public class File4 {

  public static void main(String[] args) throws Exception {
    BufferedReader r  = 
        new BufferedReader(new FileReader("data/The Lion's Story.txt"));

    r.lines()
     .map(l -> l.toUpperCase())
     .forEach(l -> System.out.println(l));
    
    r.close();
  }

}
