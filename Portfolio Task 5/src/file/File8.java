package file;

import java.io.*;
import java.util.*;
import java.util.stream.*;

//This example uses a collector to combine all the lines of a file into a list.

public class File8 {

  public static void main(String[] args) throws Exception {
    BufferedReader r  = 
        new BufferedReader(new FileReader("data/The Lion's Story.txt"));

    List<String> l = r.lines().collect(Collectors.toList());
    
    for(String line: l){
      System.out.println(line);
    }
    
    r.close();
  }

}
