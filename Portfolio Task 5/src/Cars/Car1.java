package Cars;

import java.util.Arrays;
import java.util.List;

public class Car1 {
  public static void main(String[] args) {
    List<Car_Club> table = Arrays.asList(
        new Car_Club(1, "Nissan", 12, 34, 78, 4, 567, 677, 567, 45, 34,
            7, 3, 26),
        new Car_Club(2, "Suzuki", 23, 66, 7, 8, 323, 909, 123, 56, 78, 5, 1, 12),
        new Car_Club(3, "Brabus", 12, 145, 72, 23, 342, 788, 90, 987, 67, 1,
            5, 89),
        new Car_Club(4, "Porshe", 34, 23, 3, 8, 678, 232, 746, 24, 12, 78, 8, 90),
        new Car_Club(5, "BMW", 33, 34, 2, 4, 787, 123, 236, 63, 96, 5, 9,
            78),
        new Car_Club(6, "Honda", 34, 45, 1, 3, 567, 678, 898, 897, 78, 6, 2, 56),
        new Car_Club(7, "Toyota", 32, 43, 4, 44, 123, 456, 85, 89, 9, 7, 8,
            90),
        new Car_Club(8, "Mazda", 67, 12, 2, 34, 452, 78, -89, 90, 23, 1, 4,
            33),
        new Car_Club(9, "Subaru", 78, 8, 2, 45, 678, 775, -33, 43, 65, 5, 8,
            78),
        new Car_Club(10, "Lexus", 89, 56, 3, 4, 490, 893, -786, 89, 90, 1, 2,
            85),
        new Car_Club(11, "Volvo", 34, 78, 3, 44, 345, 987, -78, 86, 45,
            2, 7, 98),
        new Car_Club(12, "RAM", 89, 1, 2, 78, 986, 1234, -678, 78, 896, 2,
            6, 9));

     table.forEach(x -> System.out.println(x));
  }

}
